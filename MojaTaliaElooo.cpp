#include <iostream>
#include <vector>

using namespace std;

class Karta
{
private:
	int wartosc;
	string nazwa;
	string kolor;
public:
	Karta(string b, string c) :nazwa(b), kolor(c) {}
	
	string zwrotNazwy()
	{
		return nazwa;
	}
	string zwrotKoloru()
	{
		return kolor;
	}

};

class Talia
{
private:
	std::vector <Karta> talia;
public:
	//tworzenie talii
	void tworzTalie()
	{
		string kolory[4] = { "kier", "karo", "pik", "trefl" };
		string figury[13] = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "W", "D", "K", "A" };
		for (int i = 0; i<4; i++)
		{
			for (int j = 0; j<13; j++)
			{
				Karta x(figury[j], kolory[i]); //generujemy kart�
				talia.push_back(x); //dodajemy do talii
			}
			
		}
	}
	void wyswietlTalie()
	{
		int sum = 0;
		for (int i = 0; i < talia.size(); i++)
		{
			std::cout << talia[i];
		}
	}

};

